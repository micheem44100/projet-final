import pygame
import pytmx
import pyscroll
from joueur import *
from joueur_2 import *
from pygame.locals import *
from pygame import *

class Jeu:

    def __init__(self):
        #créer la fenetre du jeu
        self.screen = pygame.display.set_mode((800,600))
        pygame.display.set_caption("OP")
        #music = pygame.mixer.music.load('nomdlazik.ogg')
        #pygame.mixer.music.play(-1)

        #charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame('tkt_v3.tmx')
        tmx_data2 = pytmx.util_pygame.load_pygame("map2.tmx")
        tmx_data3 = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2


        # génerer un joueur
        joueur_position = tmx_data.get_object_by_name("joueur")
        self.joueur = perso(joueur_position.x, joueur_position.y)

        # génerer un joueur
        joueur_position2 = tmx_data.get_object_by_name("joueur2")
        self.joueur_2 = perso2(joueur_position2.x, joueur_position2.y)


        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))


        #dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=6)
        self.group.add(self.joueur)
        self.group.add(self.joueur_2)

        #définir le rect de collision pour entrer dans la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
        self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        enter_map = tmx_data.get_object_by_name('entrée_map2')
        self.enter_map_rect = pygame.Rect(enter_map.x, enter_map.y, enter_map.width, enter_map.height)

        exit_map = tmx_data2.get_object_by_name('sortie_map2')
        self.exit_map_rect = pygame.Rect(exit_map.x, exit_map.y, exit_map.width, exit_map.height)

        exit_house = tmx_data3.get_object_by_name('sortie_maison')
        self.exit_house_rect = pygame.Rect(exit_house.x, exit_house.y, exit_house.width, exit_house.height)

        #initialisation du score du joueur
        self.score = 0


    def find(self):
        tmx_data = pytmx.util_pygame.load_pygame('tkt_v3.tmx')
        self.nb_objets = []
        for obj in tmx_data.objects:
            if obj.type == 'trouver':
                self.nb_objets.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        #initialisations des objets
        objet1 = tmx_data.get_object_by_name('objet1')
        self.objet1_rect = pygame.Rect(objet1.x, objet1.y, objet1.width, objet1.height)

        #verification des objets
        for sprite in self.group.sprites():
            if sprite.feet.collidelist(self.nb_objets) > -1:
                sprite.move_back()
                self.score += 1

                for obj in tmx_data.objects:
                    if obj.type == 'trouver':
                        obj.type = 'on_a_trouvé'
                print(len(self.nb_objets))
                print(self.score)

    def handle_input(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            self.joueur.move_up()
            self.joueur.change_animation('up')

        elif pressed[pygame.K_DOWN]:
            self.joueur.move_down()
            self.joueur.change_animation('down')

        elif pressed[pygame.K_LEFT]:
            self.joueur.move_left()
            self.joueur.change_animation('left')

        elif pressed[pygame.K_RIGHT]:
            self.joueur.move_right()
            self.joueur.change_animation('right')


    def handle_input2(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_w]:
            self.joueur_2.move_up()
            self.joueur_2.change_animation('up')

        elif pressed[pygame.K_s]:
            self.joueur_2.move_down()
            self.joueur_2.change_animation('down')

        elif pressed[pygame.K_a]:
            self.joueur_2.move_left()
            self.joueur_2.change_animation('left')

        elif pressed[pygame.K_d]:
            self.joueur_2.move_right()
            self.joueur_2.change_animation('right')




    def switch_house(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur)

        # définir le rect de collision pour sortir de la maison
        exit_house = tmx_data.get_object_by_name('sortie_maison')
        self.exit_house_rect = pygame.Rect(exit_house.x, exit_house.y, exit_house.width, exit_house.height)

        # recuperer le point de spawn dans la maison
        spwan_house_point = tmx_data.get_object_by_name('spawn-maison')
        self.joueur.position[0] = spwan_house_point.x
        self.joueur.position[1] = spwan_house_point.y - 20


    def switch_world(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("tkt_v3.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour entrer  de la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
        self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        #recuperer le point de spawn devant la maison
        spwan_world_point = tmx_data.get_object_by_name('sortie1')
        self.joueur.position[0] = spwan_world_point.x
        self.joueur.position[1] = spwan_world_point.y - 50


    def switch_world2(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("tkt_v3.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour entrer  de la maison
        enter_map = tmx_data.get_object_by_name('entrée_map2')
        self.enter_map_rect = pygame.Rect(enter_map.x, enter_map.y, enter_map.width, enter_map.height)

        #recuperer le point de spawn devant la maison
        spwan_world2_point = tmx_data.get_object_by_name('spawn_world2')
        self.joueur.position[0] = spwan_world2_point.x
        self.joueur.position[1] = spwan_world2_point.y - 50


    def switch_map(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("map2.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision de sortie de la map2
        exit_map = tmx_data.get_object_by_name('sortie_map2')
        self.exit_map_rect = pygame.Rect(exit_map.x, exit_map.y, exit_map.width, exit_map.height)

        #recuperer le point de spawn devant la maison
        spwan_map2_point = tmx_data.get_object_by_name('spawn_map2')
        self.joueur.position[0] = spwan_map2_point.x
        self.joueur.position[1] = spwan_map2_point.y - 50


    def update(self):

        # verifier l'entrer dans la maison
        if self.joueur.feet.colliderect(self.enter_house_rect):
            self.switch_house()


        # verifier la sortie de la maison
        if self.joueur.feet.colliderect(self.exit_house_rect):
            self.switch_world()

        # verifier la sortie de la map
        if self.joueur.feet.colliderect(self.exit_map_rect):
            self.switch_world2()

        # verifier l'entrer dans la map
        if self.joueur.feet.colliderect(self.enter_map_rect):
            self.switch_map()


        #verification collision
        for sprite in self.group.sprites():
            if sprite.feet.collidelist(self.walls) > -1:
                sprite.move_back()

        self.group.update()

    def run(self):

        clock = pygame.time.Clock()

        #boucle du jeu
        boucle = True

        while boucle:
            self.joueur.save_location()
            self.handle_input()
            self.handle_input2()
            self.update()
            self.find()
            self.group.center(self.joueur.rect.center)
            self.group.draw(self.screen)
            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    boucle = False
            clock.tick(60)
        pygame.quit()