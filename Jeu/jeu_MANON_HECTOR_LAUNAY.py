# -*- coding: utf-8 -*-
import pygame
import pytmx
import pyscroll

from dialog import *
from joueur import *
from joueur_2 import *
import time

'''
   Auteurs: MANON Michée, Launay Adam, HECTOR Marck
   Les modules pytmx, pygame et pyscroll doivent être installer pour pouvoir lancer le jeu
   vous pouvez installer tiled pour voir comment nous avons créé nos cartes (c'était très long)
   et comment nous avons attribués des noms et des types à des objets

   des sites qui nous ont aidés :
   https://pytmx.readthedocs.io/en/latest/
   https://pyscroll.readthedocs.io/en/latest/
   '''


class Jeu:
    def __init__(self):
        # créer la fenetre du jeu
        self.screen = pygame.display.set_mode((800, 600))
        self.screen_width = self.screen.get_width()
        self.screen_height = self.screen.get_height()
        self.screen_res = (self.screen_width, self.screen_height)

        self.p1_cam = pygame.surface((0, self.screen_height / 2))
        self.p2_cam = pygame.surface((self.screen_width, self.screen_height / 2))

        pygame.display.set_caption("OP")
 
        self.canvas = pygame.Surface((self.screen_res))

        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame('world.tmx')
        tmx_data2 = pytmx.util_pygame.load_pygame("map2.tmx")
        tmx_data3 = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # génerer un joueur
        joueur_position = tmx_data.get_object_by_name("joueur")
        self.joueur = persos('luffy', joueur_position.x, joueur_position.y)

        # génerer un joueur
        joueur_position2 = tmx_data.get_object_by_name("joueur2")
        self.joueur_2 = persos('luffy2', joueur_position2.x, joueur_position2.y)

        #music = pygame.mixer.music.load('we_are!.MP3')
        #pygame.mixer.music.play(-1)



        # on précise dans quelle map on se situe
        self.map = 'adventure'

        # début du chrono
        self.start = time.time()

        #génerer la boîte de dialogue
        self.dialog_box = dialogue('dialog')

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        #liste qui stock le rect de collision pour la boîte de dialogue
        self.boîte = []
        self.box1 = tmx_data.get_object_by_name('box1')
        self.boîte.append(pygame.Rect(self.box1.x, self.box1.y, self.box1.width, self.box1.height))
        print(self.boîte)

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=6)
        self.group.add(self.joueur)
        self.group.add(self.joueur_2)

        # définir le rect de collision pour entrer dans la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
        self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        # définir le rect de collision pour entrer dans la maison
        enter_map = tmx_data.get_object_by_name('entrée_map2')
        self.enter_map_rect = pygame.Rect(enter_map.x, enter_map.y, enter_map.width, enter_map.height)

        # définir le rect de collision pour entrer dans la deuxième maison
        exit_map = tmx_data2.get_object_by_name('sortie_map2')
        self.exit_map_rect = pygame.Rect(exit_map.x, exit_map.y, exit_map.width, exit_map.height)

        # définir le rect de collision pour sortir de la maison
        exit_house = tmx_data3.get_object_by_name('sortie_maison')
        self.exit_house_rect = pygame.Rect(exit_house.x, exit_house.y, exit_house.width, exit_house.height)

        # initialisations des objets
        objet1 = tmx_data.get_object_by_name('objet1')
        objet2 = tmx_data.get_object_by_name('objet2')
        objet3 = tmx_data3.get_object_by_name('objet3')
        objet4 = tmx_data2.get_object_by_name('objet4')
        objet5 = tmx_data2.get_object_by_name('objet5')
        objet6 = tmx_data2.get_object_by_name('objet6')
        objet7 = tmx_data.get_object_by_name('objet7')
        objet8 = tmx_data2.get_object_by_name('objet8')
        objet9 = tmx_data2.get_object_by_name('objet9')
        objet10 = tmx_data2.get_object_by_name('objet10')

        # récuperations des positions des objets
        self.objet1_rect = pygame.Rect(objet1.x, objet1.y, objet1.width, objet1.height)
        self.objet2_rect = pygame.Rect(objet2.x, objet2.y, objet2.width, objet2.height)
        self.objet3_rect = pygame.Rect(objet3.x, objet3.y, objet3.width, objet3.height)
        self.objet4_rect = pygame.Rect(objet4.x, objet4.y, objet4.width, objet4.height)
        self.objet5_rect = pygame.Rect(objet5.x, objet5.y, objet5.width, objet5.height)
        self.objet6_rect = pygame.Rect(objet6.x, objet6.y, objet6.width, objet6.height)
        self.objet7_rect = pygame.Rect(objet7.x, objet7.y, objet7.width, objet7.height)
        self.objet8_rect = pygame.Rect(objet8.x, objet8.y, objet8.width, objet8.height)
        self.objet9_rect = pygame.Rect(objet9.x, objet9.y, objet9.width, objet9.height)
        self.objet10_rect = pygame.Rect(objet10.x, objet10.y, objet10.width, objet10.height)

        # on mets tous les objets dans une liste pour pouvoir la parcourir plus tard
        self.positions_des_objets = [
            self.objet1_rect,
            self.objet2_rect,
            self.objet3_rect,
            self.objet4_rect,
            self.objet5_rect,
            self.objet6_rect,
            self.objet7_rect,
            self.objet8_rect,
            self.objet9_rect,
            self.objet10_rect,
        ]
        self.nouvelle_liste = self.positions_des_objets

    def touches(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            self.joueur.move_up()
            self.joueur.change_animation('up')

        elif pressed[pygame.K_DOWN]:
            self.joueur.move_down()
            self.joueur.change_animation('down')

        elif pressed[pygame.K_LEFT]:
            self.joueur.move_left()
            self.joueur.change_animation('left')

        elif pressed[pygame.K_RIGHT]:
            self.joueur.move_right()
            self.joueur.change_animation('right')

        elif pressed[pygame.K_j]:
            self.joueur.change_animation('attack_right')
            self.joueur.attack_right()

    def touches2(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_w]:
            self.joueur_2.move_up()
            self.joueur_2.change_animation('up')

        elif pressed[pygame.K_s]:
            self.joueur_2.move_down()
            self.joueur_2.change_animation('down')

        elif pressed[pygame.K_a]:
            self.joueur_2.move_left()
            self.joueur_2.change_animation('left')

        elif pressed[pygame.K_d]:
            self.joueur_2.move_right()
            self.joueur_2.change_animation('right')

    def switch_maison(self):
        # charger la carte(tmx)
        tmx_data3 = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data3)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        self.map = 'maison'

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data3.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=6)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour sortir de la maison
        exit_house = tmx_data3.get_object_by_name('sortie_maison')
        self.exit_house_rect = pygame.Rect(exit_house.x, exit_house.y, exit_house.width, exit_house.height)

        # recuperer le point de spawn dans la maison
        spwan_house_point = tmx_data3.get_object_by_name('spawn-maison')
        self.joueur.position[0] = spwan_house_point.x
        self.joueur.position[1] = spwan_house_point.y
        self.joueur.save_location()

    def switch_world(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("world.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        self.map = 'adventure'

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision" or obj.type == 'box':
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=6)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour entrer  de la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
        self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        # recuperer le point de spawn devant la maison
        spwan_world_point = tmx_data.get_object_by_name('sortie1')
        self.joueur.position[0] = spwan_world_point.x
        self.joueur.position[1] = spwan_world_point.y - 50
        self.joueur.save_location()

    def switch_world2(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("world.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        self.map = 'adventure'

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=6)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour entrer  de la maison
        enter_map = tmx_data.get_object_by_name('entrée_map2')
        self.enter_map_rect = pygame.Rect(enter_map.x, enter_map.y, enter_map.width, enter_map.height)

        # recuperer le point de spawn devant la maison
        spwan_world2_point = tmx_data.get_object_by_name('spawn_world2')
        self.joueur.position[0] = spwan_world2_point.x
        self.joueur.position[1] = spwan_world2_point.y
        self.joueur.save_location()

    def switch_map(self):
        # charger la carte(tmx)
        tmx_data2 = pytmx.util_pygame.load_pygame("map2.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data2)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        self.map = 'map2'

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data2.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=6)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision de sortie de la map2
        exit_map = tmx_data2.get_object_by_name('sortie_map2')
        self.exit_map_rect = pygame.Rect(exit_map.x, exit_map.y, exit_map.width, exit_map.height)

        # recuperer le point de spawn devant la maison
        spwan_map2_point = tmx_data2.get_object_by_name('spawn_map2')
        self.joueur.position[0] = spwan_map2_point.x
        self.joueur.position[1] = spwan_map2_point.y
        self.joueur.save_location()

    def update(self):

        self.group.update()
        font = pygame.font.Font(None, 24)
        self.text_print_score = font.render('', 1, (255, 255, 255))

        # verifier l'entrer dans la maison
        if self.map == 'adventure' and self.joueur.feet.colliderect(self.enter_house_rect):
            self.joueur.save_location()
            self.switch_maison()

        # verifier la sortie de la maison
        if self.map == 'maison' and self.joueur.feet.colliderect(self.exit_house_rect):
            self.joueur.save_location()
            self.switch_world()

        # verifier la sortie de la map
        if self.map == 'map2' and self.joueur.feet.colliderect(self.exit_map_rect):
            self.joueur.save_location()
            self.switch_world2()

        # verifier l'entrer dans la map
        if self.map == 'adventure' and self.joueur.feet.colliderect(self.enter_map_rect):
            self.joueur.save_location()
            self.switch_map()

        # verification collision
        for sprite in self.group.sprites():
            if sprite.feet.collidelist(self.walls) > -1:
                sprite.move_back()

        # verification des objets
        for objet in self.positions_des_objets:
            if self.joueur.feet.colliderect(objet):
                self.nouvelle_liste.remove(objet)
                self.positions_des_objets = self.nouvelle_liste

        # initialisation des textes
        self.text_2player = font.render('', 1, (0, 0, 0))
        self.text_2player_keys = font.render('', 1, (0, 0, 0))
        self.text_final = font.render('', 1, (0, 0, 0))
        self.text = font.render(f"Il reste {len(self.positions_des_objets)} objets à trouver", 1, (255, 255, 255))
        self.end = time.time()
        self.temps_restant = 60 - round(self.end - self.start, 1)
        self.text_time = font.render(f'Vous avez {self.temps_restant} pour trouver tout les objet', 1, (255, 255, 255))

        if self.temps_restant <= 0 or len(self.positions_des_objets) == 0:
            self.score = abs(len(self.positions_des_objets) - 10)
            self.text_print_score = font.render(
                f"félicitation! Vous avez obtenu un score de: {self.score} ", 1,
                (255, 255, 255))

            self.text = font.render('', 1, (0, 0, 0))
            self.text_time = font.render('', 1, (0, 0, 0))

            if self.score == 10:
                self.text_print_score = font.render('', 1, (0, 0, 0))
                self.text_final = font.render('Vous êtes désormais un pirate libre! Vous pouvez visiter ce monde', 1, (255, 255, 255))
                self.text_2player = font.render('Les touches du deuxième joueur sont:', 1, (124, 10, 60))
                self.text_2player_keys = font.render('W(haut), A(gauche), S(bas), D(droite)', 1, (124, 10, 60))



    def run(self):
        clock = pygame.time.Clock()

        # boucle du jeu
        boucle = True

        while boucle:
            self.joueur.save_location()
            self.touches()
            self.touches2()
            self.update()

            p1_view = self.group.center(self.joueur.rect.center)
            self.screen.blit(self.p1_cam, (0, self.screen_height / 2))

            p2_view = self.group.center(self.joueur_2.rect.center)
            self.screen.blit(self.p2_cam, (self.screen_width, self.screen_height / 2))

            self.group.draw(self.screen)           

            pygame.draw.line(self.screen,  (255, 255, 255) , (0, self.screen_height / 2), (self.screen_width, self.screen_height / 2), 10)

            self.screen.blit(self.text, (5, 10))
            self.screen.blit(self.text_print_score, (self.screen_width // 2.5, 10))
            self.screen.blit(self.text_time, (10, 50))
            self.screen.blit(self.text_final, (self.screen_width // 2.8, 10))
            self.screen.blit(self.text_2player, (self.screen_width - 300, self.screen_height - 150))
            self.screen.blit(self.text_2player_keys, (self.screen_width - 300, self.screen_height - 100))

            self.dialog_box.render(self.screen)

            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    boucle = False

                if event.type == pygame.KEYDOWN:
                    for box in self.boîte:
                        if event.key == pygame.K_SPACE  and self.joueur.feet.colliderect(box):
                            self.dialog_box.execute()

            clock.tick(60)
        pygame.quit()
