import pygame

class anim(pygame.sprite.Sprite):

    def __init__(self, name):
        super().__init__()
        self.sprite_sheet = pygame.image.load(f'{name}.png')
        self.animation_index = 0
        self.clock = 0
        self.images = {
            'down': self.get_images(0),
            'left': self.get_images(40),
            'right': self.get_images(40 * 2),
            'up': self.get_images(40 * 3),
            'static_down': self.get_image(0, 0),
            'static_up': self.get_image(0, 40 * 3),
            'static_left': self.get_image(0, 40),
            'static_right': self.get_image(0, 40 * 2),
            'attack_right': self.get_image(0, 80)
        }
        self.speed = 5

    def change_animation(self, name):
        self.image = self.images[name][self.animation_index]
        self.image.set_colorkey((0, 0, 0))
        self.clock += self.speed * 8

        if self.clock >= 200:
            self.animation_index += 1 #passer à l'image suivante
            if self.animation_index >= len(self.images[name]):
                self.animation_index = 0
            self.clock = 0



    def get_images(self, y):
        images = []
        for i in range(0, 5):

            x = i * 40
            image = self.get_image(x, y)
            images.append(image)

        return images


    def get_image(self, x, y):
        image = pygame.Surface([40, 40])
        image.blit(self.sprite_sheet, (0, 0), (x, y, 40, 40))
        return image
