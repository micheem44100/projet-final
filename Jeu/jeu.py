import pygame
import pytmx
import pyscroll
from joueur import *
from joueur_2 import *
from pygame.locals import *
from pygame import *

class Jeu:

    def __init__(self):
        #créer la fenetre du jeu
        self.screen = pygame.display.set_mode((800,600))
        pygame.display.set_caption("OP")
        music = pygame.mixer.music.load('we_are!.MP3')
        pygame.mixer.music.play(-1)



        self.map = 'world'

        #charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame('tkt_v3.tmx')
        tmx_data2 = pytmx.util_pygame.load_pygame("map2.tmx")
        tmx_data3 = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # génerer un joueur
        joueur_position = tmx_data.get_object_by_name("joueur")
        self.joueur = perso(joueur_position.x, joueur_position.y)

        # génerer un joueur
        joueur_position2 = tmx_data.get_object_by_name("joueur2")
        self.joueur_2 = perso2(joueur_position2.x, joueur_position2.y)


        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        #dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer = map_layer, default_layer = 8)
        self.group.add(self.joueur)
        self.group.add(self.joueur_2)

        #définir le rect de collision pour entrer dans la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
        self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        enter_map = tmx_data.get_object_by_name('entrée_map2')
        self.enter_map_rect = pygame.Rect(enter_map.x, enter_map.y, enter_map.width, enter_map.height)

        exit_map = tmx_data2.get_object_by_name('sortie_map2')
        self.exit_map_rect = pygame.Rect(exit_map.x, exit_map.y, exit_map.width, exit_map.height)

        exit_house = tmx_data3.get_object_by_name('sortie_maison')
        self.exit_house_rect = pygame.Rect(exit_house.x, exit_house.y, exit_house.width, exit_house.height)


        #initialisations des objets
        objet1 = tmx_data.get_object_by_name('objet1')
        objet2 = tmx_data.get_object_by_name('objet2')
        objet3 = tmx_data.get_object_by_name('objet3')
        objet4 = tmx_data.get_object_by_name('objet4')
        objet5 = tmx_data.get_object_by_name('objet5')
        objet6 = tmx_data.get_object_by_name('objet6')
        objet7 = tmx_data.get_object_by_name('objet7')
        objet8 = tmx_data.get_object_by_name('objet8')
        objet9 = tmx_data.get_object_by_name('objet9')
        objet10 = tmx_data.get_object_by_name('objet10')


        #récuperations des positions des objets
        self.objet1_rect = pygame.Rect(objet1.x, objet1.y, objet1.width, objet1.height)
        self.objet2_rect = pygame.Rect(objet2.x, objet2.y, objet2.width, objet2.height)
        self.objet3_rect = pygame.Rect(objet3.x, objet3.y, objet3.width, objet3.height)
        self.objet4_rect = pygame.Rect(objet4.x, objet4.y, objet4.width, objet4.height)
        self.objet5_rect = pygame.Rect(objet5.x, objet5.y, objet5.width, objet5.height)
        self.objet6_rect = pygame.Rect(objet6.x, objet6.y, objet6.width, objet6.height)
        self.objet7_rect = pygame.Rect(objet7.x, objet7.y, objet7.width, objet7.height)
        self.objet8_rect = pygame.Rect(objet8.x, objet8.y, objet8.width, objet8.height)
        self.objet9_rect = pygame.Rect(objet9.x, objet9.y, objet9.width, objet9.height)
        self.objet10_rect = pygame.Rect(objet10.x, objet10.y, objet10.width, objet10.height)

        #on mets tous les objets dans une liste pour pouvoir la parcourir plus tard
        self.positions_des_objets = [
        self.objet1_rect,
        self.objet2_rect,
        self.objet3_rect,
        self.objet4_rect,
        self.objet5_rect,
        self.objet6_rect,
        self.objet7_rect,
        self.objet8_rect,
        self.objet9_rect,
        self.objet10_rect,
        ]
        self.nouvelle_liste = self.positions_des_objets




    def handle_input(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            self.joueur.move_up()
            self.joueur.change_animation('up')

        elif pressed[pygame.K_DOWN]:
            self.joueur.move_down()
            self.joueur.change_animation('down')

        elif pressed[pygame.K_LEFT]:
            self.joueur.move_left()
            self.joueur.change_animation('left')

        elif pressed[pygame.K_RIGHT]:
            self.joueur.move_right()
            self.joueur.change_animation('right')


    def handle_input2(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_w]:
            self.joueur_2.move_up()
            self.joueur_2.change_animation('up')

        elif pressed[pygame.K_s]:
            self.joueur_2.move_down()
            self.joueur_2.change_animation('down')

        elif pressed[pygame.K_a]:
            self.joueur_2.move_left()
            self.joueur_2.change_animation('left')

        elif pressed[pygame.K_d]:
            self.joueur_2.move_right()
            self.joueur_2.change_animation('right')




    def switch_house(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2


        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))


        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer = map_layer, default_layer = 8)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour sortir de la maison
        exit_house = tmx_data.get_object_by_name('sortie_maison')
        self.exit_house_rect = pygame.Rect(exit_house.x, exit_house.y, exit_house.width, exit_house.height)

        # recuperer le point de spawn dans la maison
        spwan_house_point = tmx_data.get_object_by_name('spawn-maison')
        self.joueur.position[0] = spwan_house_point.x
        self.joueur.position[1] = spwan_house_point.y


    def switch_world(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("tkt_v3.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))



        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer = map_layer, default_layer = 8)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour entrer  de la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
        self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        #recuperer le point de spawn devant la maison
        spwan_world_point = tmx_data.get_object_by_name('sortie1')
        self.joueur.position[0] = spwan_world_point.x
        self.joueur.position[1] = spwan_world_point.y - 50


    def switch_world2(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("tkt_v3.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2


        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))


        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer = map_layer, default_layer = 8)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision pour entrer  de la maison
        enter_map = tmx_data.get_object_by_name('entrée_map2')
        self.enter_map_rect = pygame.Rect(enter_map.x, enter_map.y, enter_map.width, enter_map.height)

        #recuperer le point de spawn devant la maison
        spwan_world2_point = tmx_data.get_object_by_name('spawn_world2')
        self.joueur.position[0] = spwan_world2_point.x
        self.joueur.position[1] = spwan_world2_point.y


    def switch_map(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("map2.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2


        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))


        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer = map_layer, default_layer = 8)
        self.group.add(self.joueur, self.joueur_2)

        # définir le rect de collision de sortie de la map2
        exit_map = tmx_data.get_object_by_name('sortie_map2')
        self.exit_map_rect = pygame.Rect(exit_map.x, exit_map.y, exit_map.width, exit_map.height)

        #recuperer le point de spawn devant la maison
        spwan_map2_point = tmx_data.get_object_by_name('spawn_map2')
        self.joueur.position[0] = spwan_map2_point.x
        self.joueur.position[1] = spwan_map2_point.y


    def update(self):


        self.group.update()

        tmx_data = pytmx.util_pygame.load_pygame('tkt_v3.tmx')


        # verifier l'entrer dans la maison
        if self.map == 'world' and self.joueur.feet.colliderect(self.enter_house_rect):
            self.switch_house()
            self.map = 'house'

        # verifier la sortie de la maison
        if self.map == 'house' and self.joueur.feet.colliderect(self.exit_house_rect):
            self.switch_world()
            self.map = 'world'


        # verifier la sortie de la map
        if self.map == 'map' and self.joueur.feet.colliderect(self.exit_map_rect):
            self.switch_world2()
            self.map = 'world'

        # verifier l'entrer dans la map
        if self.map == 'world' and self.joueur.feet.colliderect(self.enter_map_rect):
            self.switch_map()
            self.map = 'map'


        #verification collision
        for sprite in self.group.sprites():
            if sprite.feet.collidelist(self.walls) > -1:
                sprite.move_back()


        #verification des objets
        for objet in self.positions_des_objets:
            if self.joueur.feet.colliderect(objet):
                self.nouvelle_liste.remove(objet)
                self.positions_des_objets = self.nouvelle_liste

        #affichage du texte
        font = pygame.font.Font(NONE, 24)
        self.text = font.render(f"Il reste {len(self.positions_des_objets)} objets à trouver",1,(255,255,255))


    def run(self):

        clock = pygame.time.Clock()

        #boucle du jeu
        boucle = True

        while boucle:
            self.joueur.save_location()
            self.handle_input()
            self.handle_input2()
            self.update()
            self.group.center(self.joueur.rect.center)
            self.group.draw(self.screen)
            self.screen.blit(self.text, (0, 10))
            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    boucle = False

            clock.tick(60)
        pygame.quit()