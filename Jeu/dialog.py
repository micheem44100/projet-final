import pygame



class dialogue:

    X_POSITION = 60
    Y_POSITION = 470

    def __init__(self, name):
        self.box = pygame.image.load(f'{name}.png')
        self.box = pygame.transform.scale(self.box, (700, 100))
        self.texts = ["Bienvenue dans ce nouveau monde!", 'we tkt', 'dbz >> all anim']
        self.text_index = 0
        self.letter_index = 0
        self.clock = 0
        self.font = pygame.font.Font(None, 50)
        self.reading = False

    def execute(self):
        if self.reading:
            self.next_text()
        else:
            self.reading = True
            self.text_index = 0

    def render(self, screen):
        if self.reading == True:
            self.clock += self.letter_index * 2
            self.letter_index += 1
            if self.clock >= 10:
                if self.letter_index >= len(self.texts[self.text_index]):
                    self.letter_index = self.letter_index

                screen.blit(self.box, (self.X_POSITION, self.Y_POSITION))
                self.welcome = self.font.render(self.texts[self.text_index][0: self.letter_index], False, (0, 0, 0))
                screen.blit(self.welcome, (self.X_POSITION + 50, self.Y_POSITION + 30))
                

    def next_text(self):
        
        self.text_index += 1
        self.letter_index = 0

        if self.text_index >= len(self.texts):
            #fermer ldialogue
            self.reading = False


